#!/usr/bin/env python
# coding: utf-8

# In[9]:


import numpy as np


# In[2]:


my_list = [1,2,3]
my_list


# In[10]:


np.array(my_list)


# In[11]:


my_matrix = [[1,2,3],[4,5,6],[7,8,9]]
my_matrix


# In[12]:


np.array(my_matrix)


# In[13]:


np.arange(0,10)


# In[14]:


np.arange(0,11,2)


# In[15]:


np.zeros(3)


# In[16]:


np.zeros((5,5))


# In[17]:


np.ones(3)


# In[18]:


np.ones((3,3))


# In[19]:


np.linspace(0,10,3)


# In[20]:


np.linspace(0,10,50)


# In[21]:


np.eye(4)


# In[22]:


np.random.rand(2)


# In[23]:


np.random.rand(5,5)


# In[24]:


np.random.randn(2)


# In[25]:


np.random.randn(5,5)


# In[26]:


np.random.randint(1,100)


# In[27]:


np.random.randint(1,100,10)


# In[28]:


arr = np.arange(25)
ranarr = np.random.randint(0,50,10)


# In[29]:


arr


# In[30]:


ranarr


# In[31]:


arr.reshape(5,5)


# In[32]:


ranarr


# In[33]:


ranarr.max()


# In[35]:


ranarr.argmax()


# In[36]:


ranarr.min()


# In[37]:


ranarr.argmin()


# In[38]:


arr.shape


# In[39]:


arr.reshape(1,25)


# In[40]:


arr.reshape(1,25).shape


# In[42]:


arr.reshape(25,1)


# In[43]:


arr.reshape(25,1).shape


# In[44]:


arr.dtype


# In[45]:


arr = np.arange(0,11)


# In[46]:


arr


# In[50]:


arr[8]


# In[47]:


arr[1:5]


# In[48]:


arr[0:5]


# In[49]:


arr[0:5] = 100
arr


# In[51]:


arr[0:5] = 100
arr


# In[52]:


arr = np.arange(0,11)
arr


# In[53]:


slice_of_arr = arr[0:6]
slice_of_arr


# In[54]:


slice_of_arr[:] = 99
slice_of_arr


# In[55]:


arr


# In[56]:


arr_copy = arr.copy()
arr_copy


# In[57]:


arr_2d = np.array(([5,10,15],[20,25,30],[35,40,45]))
arr_2d


# In[58]:


arr_2d[1]


# In[59]:


arr_2d[1][0]


# In[60]:


arr_2d[1,0]


# In[61]:


arr_2d[:2,1:]


# In[62]:


arr_2d[2]


# In[63]:


arr_2d[2,:]


# In[64]:


arr = np.arange(1,11)
arr


# In[65]:


arr > 4


# In[66]:


bool_arr = arr>4


# In[67]:


bool_arr


# In[68]:


arr[arr>2]


# In[69]:


x = 2
arr[arr>2]


# In[70]:


arr = np.arange(0,10)


# In[71]:


arr + arr


# In[72]:


arr * arr


# In[73]:


arr - arr


# In[75]:


arr/arr


# In[76]:


1/arr


# In[77]:


arr**3


# In[78]:


np.sqrt(arr)


# In[79]:


np.exp(arr)


# In[80]:


np.max(arr)


# In[81]:


np.sin(arr)


# In[82]:


np.log(arr)


# In[ ]:




