#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np


# In[3]:


# creat an array of np.zeros(10)
np.zeros (10)


# In[4]:


# creat an array of 10 ones
np.ones (10)


# In[9]:


# creat an array of 10 fives
np.ones (10)*5


# In[10]:


# creat an array of integers from 10 to 50
np.arange (10,50)


# In[11]:


# creat an array of even integers from 10 to 50
np.arange (10,50,2)


# In[13]:


#create a 3x3 matrix with velues ranging from 0 to 8
arr = np.arange (9)
arr.reshape (3,3)


# In[14]:


# create a 3x3 indentity matrix
np.eye(3)


# In[18]:


# use numpy to generate a random number between 0 and 1
np.random.normal (0,1,1)


# In[20]:


# use numpy to genarate an array of 25 random numbers sampled from a standard normal distribution
np.random.randn (5,5)


# In[7]:


arr=np.linspace(0.01,1.00,100)
arr.reshape((10,10))


# In[25]:


#create an array of 20 linearly spaced points between 0 and 1
np.linspace (0,1,20)


# In[27]:


# here is thegiven matrix called mat
# use it for following tasks
mat = np.arange (1,26).reshape (5,5)
mat


# In[41]:


# here is thegiven matrix called mat
# use it for following tasks
# be able to see the output any more
mat[2:,1:]


# In[43]:


# here is thegiven matrix called mat
# use it for following tasks
# be able to see the output any more
mat [3,4]


# In[105]:


# here is thegiven matrix called mat
# use it for following tasks
# be able to see the output any more
mat [:3,1:2:]


# In[72]:


# here is thegiven matrix called mat
# use it for following tasks
# be able to see the output any more
mat[4]


# In[82]:


# here is thegiven matrix called mat
# use it for following tasks
# be able to see the output any more
mat[3:,0:]


# In[83]:


#get the sum of all the values in mat
np.sum (mat)


# In[86]:


# get the standard deviation of the values in mat
np.std (mat)


# In[6]:


#get the numpy sum of all the columns in mat
import numpy as np
mat = np.arange (1,26).reshape (5,5)
np.sum(mat, axis = 0)


# In[ ]:




